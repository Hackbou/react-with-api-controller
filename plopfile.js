const Case = require('case')

module.exports = function (plop) {
    // controller generator
    plop.setGenerator('generate', {
        description: 'Code faster',
        prompts: [{
            type: 'input',
            name: 'name',
            message: 'Name your ressource :'
        }],
        actions: [
            {
                type: 'add',
                path: 'src/redux/apiController/{{name}}Api.js',
                templateFile: 'src/templates/apiController.template.hbs',
                skipIfExists: true
            },
            {
                type: 'add',
                path: 'src/redux/reducers/{{name}}Slice.js',
                templateFile: 'src/templates/reducer.template.hbs',
                skipIfExists: true
            },

        ]
    });

    plop.setHelper("pascalCase", (str) => Case.pascal(str))
    plop.setHelper("camelCase", (str) => Case.camel(str))
};