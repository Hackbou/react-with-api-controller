import React from 'react'
import { NavLink } from 'react-router-dom'
import styled from 'styled-components'

const LinkItem = styled(NavLink)`
    position: relative;
    display: flex;
    align-items: center;
    flex-wrap: nowrap;
    margin-bottom: 13px;
    height: 35px;
    overflow: hidden;
    textDecoration: none;
    color: ${({ color }) => color};
    &:hover{
        color: ${({ colorOnHover }) => colorOnHover};
    }
    &.active{
        color: white;
        background-color: ${({ colorOnActive }) => colorOnActive}
    };
`

const SidebarItem = ({ linkItem }) => {
    const { path, label } = linkItem
    return (
        <LinkItem
            color={'rgba(0,0,0,0.5)'}
            colorOnHover={'blue'}
            colorOnActive={'rgba(0,5,10,.5)'}
            to={path}
        >
            <div
                style={{
                    height: 35,
                    width: 40,
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                }}
            >
                <linkItem.icon
                    size={22}
                    className="icon"
                />
            </div>
            <p
                style={{
                    position: "absolute",
                    left: 45,
                    fontSize: 17,
                    width: "100%",
                }}
            >
                {label}
            </p>
        </LinkItem>
    )
}

export default SidebarItem