import React from "react";
import { Navigate, Outlet, useLocation } from "react-router-dom";
import getCurrentUser from "../hooks/getCurrentUser";
import NavbarComponent from "../layouts/navbar/NavbarComponent";
import SidebarComponent from "../layouts/sidebar/SidebarComponent";
import SpliTemplateScreen from "../layouts/SplitTemplateScreen";
import { NOTFOUND } from "../utils/navigationPaths";

function RequireAuth({ role }) {
  const user = getCurrentUser();

  const location = useLocation();

  return user?.role === role ? (
    <SpliTemplateScreen>
      <NavbarComponent />
      <SidebarComponent />
      <Outlet />
    </SpliTemplateScreen>
  ) : (
    <Navigate to={NOTFOUND} state={{ from: location }} replace />
  );
}

export default RequireAuth;
