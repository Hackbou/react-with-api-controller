import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import RequireAuth from "../components/RequireAuth";
import { NOTFOUND } from "../utils/navigationPaths";
import { privateRoutes, publicRoutes } from "./navigationRoutes";

function Roots() {
  return (
    <BrowserRouter>
      <Routes>
        {publicRoutes?.map((item, key) => (
          <Route path={item?.path} element={item.element} key={key} end />
        ))}

        <Route element={<RequireAuth role={"user"} />}>
          {privateRoutes?.map((item, key) => (
            <Route path={item?.path} element={item.element} key={key} end />
          ))}
        </Route>

        <Route path={NOTFOUND} element={<div>Page 404</div>} />
      </Routes>
    </BrowserRouter>
  );
}

export default Roots;
