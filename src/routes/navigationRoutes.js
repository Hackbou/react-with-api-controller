import LoginPage from "pages/auth/LoginPage";
import RegisterPage from "pages/auth/RegisterPage";
import HomePage from "../pages/home/HomePage";
import { ACCEUIL_PATH, LOGIN_PATH, REGISTER_PATH } from "../utils/navigationPaths";

export const publicRoutes = [
  // {
  //   path: "*",
  //   element: <Navigate to={LOGIN} />,
  // },
  {
    path: LOGIN_PATH,
    element: <LoginPage />,
  },
  {
    path: REGISTER_PATH,
    element: <RegisterPage />
  }
];

export const privateRoutes = [
  {
    path: ACCEUIL_PATH,
    element: <HomePage />,
  },
];
