import {
  FaHome,
  FaServer,
  FaProductHunt,
  FaHeart
} from "react-icons/fa";

export const links = [
  { path: "/", icon: FaHome, label: "Home" },
  { path: "/p", icon: FaProductHunt, label: "Products" },
  { path: "/s", icon: FaServer, label: "Settings" },
  { path: "/c", icon: FaHeart, label: "commandes" },
];
