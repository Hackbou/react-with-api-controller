import { combineReducers, configureStore } from "@reduxjs/toolkit";
import { persistReducer, persistStore } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import users from "../reducers/users";


const persistConfig = {
    key: 'root',
    storage,
}


const rootReducer = combineReducers({
    users,
})

const persistedReducer = persistReducer(persistConfig, rootReducer)

export const store = configureStore({
    reducer: persistedReducer,
    devTools: true,
})

export const persistor = persistStore(store)
