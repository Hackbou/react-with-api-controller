import React from "react";
// import styles from "assets/styles.module.css";
import { FaSignOutAlt } from "react-icons/fa";

function Logout() {
  return <FaSignOutAlt style={{ margin: "0 10px" }} size={18} />;
}

export default Logout;
