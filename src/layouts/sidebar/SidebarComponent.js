import React from "react";
import SidebarItem from "../../components/SidebarItem";
import { links } from "../../data/links";
import ResourceDataMap from "../../components/ResourceDataMap";

const SidebarComponent = ({ bgColor, sideBarContainerStyles }) => {
  return (
    <div
      style={{
        backgroundColor: bgColor ? bgColor : "#fff",
        height: "100%",
        padding: "25px 10px",
        borderRight: "1px solid rgba(0,0,0,0.1)",
        ...sideBarContainerStyles,
      }}
    >
      <ResourceDataMap
        resourceData={links}
        resourceItem={SidebarItem}
        resourceName="linkItem"
      />
    </div>
  );
};

export default SidebarComponent;
